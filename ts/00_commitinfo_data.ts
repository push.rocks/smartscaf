/**
 * autocreated commitinfo by @pushrocks/commitinfo
 */
export const commitinfo = {
  name: '@pushrocks/smartscaf',
  version: '4.0.5',
  description: 'scaffold projects quickly'
}
